class RenameEstimearedHourstoEstimatedHours < ActiveRecord::Migration[5.0]
  def change
    rename_column :proposals ,:estimeated_hours, :estimated_hours
  end
end
